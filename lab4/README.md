#### CD
![CD](img/cd.png "CD")

#### SD

Авторизация:

![SD_Auth](img/sd_auth.png "SD_Auth")

Добавление/редактирование фильма:

![SD_Film_Edit](img/sd_film_edit.png "SD_Film_Edit")

#### DDL

``` sql

CREATE DATABASE IF NOT EXISTS filmCatalog;

USE filmCatalog;

CREATE TABLE Users(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(24) NOT NULL,
    passwordHash VARCHAR(33) NOT NULL,
    adminLevel INT NOT NULL,
    regDate DATETIME NOT NULL
);

INSERT INTO Users VALUES (1, 'admin', '9d24cce127181493d7793b7092348847', 1, '2019-09-28 23:03:03');

CREATE TABLE Films(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(70) NOT NULL,
    poster VARCHAR(14) NOT NULL,
    ageLimit INT NOT NULL,
    endTime DATE NOT NULL,
    year INT NOT NULL
);

CREATE TABLE Actors(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(32) NOT NULL,
    birthDate DATE,
    biography VARCHAR(200)
);

CREATE TABLE Directors(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(32) NOT NULL
);

CREATE TABLE Relations(
    name VARCHAR(32) NOT NULL,
    owner_id INT NOT NULL,
    film_id INT NOT NULL
);

CREATE TABLE Bookmarks(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    film_id INT NOT NULL,
    owner VARCHAR(32) NOT NULL,
    addedTime DATETIME NOT NULL
);

```

#### Диаграмма классов
![Classes](img/classes.png "Classes")

#### Приложение:
Главная страница:
![Main_Page](img/main.png "Main_Page")

Регистрация:
![Registration](img/registration.png "Registration")

Страница фильма:
![Film](img/film.png "Film")

Редактирование фильма:
![Film_Edit](img/film_edit.png "Film_Edit")

Редактирование актера:
![Actor_Edit](img/actor_edit.png "Actor_Edit")

